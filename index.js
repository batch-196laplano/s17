/*
	1. Create a function which is able to prompt the user to provide their fullname, age, and location.
		-use prompt() and store the returned value into a function scoped variable within the function.
		-display the user's inputs in messages in the console.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
*/
	
	//first function here:
	let printUserInputs = function printUserInfo(){
		alert("Hi! Please provide the following information.");
		let fullName = prompt("Enter your full name: "); 
		let age = prompt("Enter your age: "); 
		let location = prompt("Enter your location: ");

		console.log("Hi " + fullName)
		console.log(fullName + ' \'s age is ' + age ); 
		console.log(location + ' is located at ' + location);
	};
	printUserInputs();
/*
	2. Create a function which is able to print/display your top 5 favorite bands/musical artists.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/

	//second function here:
	function getFavoriteBands(){
		let bands = ['Typecast','Franco','Urbandub','Chicosci','Slapshock'];
		console.log(bands);
	}
	getFavoriteBands();
/*
	3. Create a function which is able to print/display your top 5 favorite movies of all time and show Rotten Tomatoes rating.
		-Look up the Rotten Tomatoes rating of your favorite movies and display it along with the title of your favorite movie.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/
	
	//third function here:
	function getFavoriteMovies(){
	let movie1 = "Taken 1";
	let tomatoMeter1 = 59;
	let movie2 = "Taken 2";
	let tomatoMeter2 = 22;
	let movie3 = "Taken 3";
	let tomatoMeter3 = 13;
	let movie4 = "Iron Man 2";
	let tomatoMeter4 = 72;
	let movie5 = "Iron Man 3";
	let tomatoMeter5 = 79;

	console.log("1. " + movie1);
	console.log("Tomatometer for " + movie1 + ": "+ tomatoMeter1 + "%");
	console.log("2. " + movie2);
	console.log("Tomatometer for " + movie2 + ": "+ tomatoMeter2 + "%");
	console.log("3. " + movie3);
	console.log("Tomatometer for " + movie3 + ": "+ tomatoMeter3 + "%");
	console.log("4. " + movie4);
	console.log("Tomatometer for " + movie4 + ": "+ tomatoMeter4 + "%");
	console.log("5. " + movie5);
	console.log("Tomatometer for " + movie5 + ": "+ tomatoMeter5 + "%");

}
getFavoriteMovies();

/*
	4. Debugging Practice - Debug the following codes and functions to avoid errors.
		-check the variable names
		-check the variable scope
		-check function invocation/declaration
		-comment out unusable codes.
*/


let printFriends = function printUsers(){
	alert("Hi! Please add the names of your friends.");
	let friend1 = prompt("Enter your first friend's name:"); 
	let friend2 = prompt("Enter your second friend's name:"); 
	let friend3 = prompt("Enter your third friend's name:");

	console.log("You are friends with:")
	console.log(friend1); 
	console.log(friend2); 
	console.log(friend3); 

};
printFriends();